//
//  GalleryCell.swift
//  RemoteGallery
//
//  Created by Andrei Kliuchenia on 20.02.22.
//

import UIKit

class GalleryCell: UICollectionViewCell {


    @IBOutlet weak var contentCellView: UIView!

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!

    @IBOutlet weak var photoUrlLabel: UILabel!
    @IBOutlet weak var userUrlLabel: UILabel!

    

    var delegate: CellDelegate?

    var userName: String? {
        didSet {
            userNameLabel.text = userName
        }
    }

    var photoUrlText: String? {
        didSet {
            photoUrlLabel.text = photoUrlText
        }
    }

    var userUrlText: String? {
        didSet {
            userUrlLabel.text = userUrlText
        }
    }

    @IBAction func photoUrlClicked(_ sender: UIButton) {
        delegate?.openLink(url: photoUrlText ?? "")
    }

    @IBAction func userUrlClicked(_ sender: UIButton) {
        delegate?.openLink(url: userUrlText ?? "")
    }
}

extension CALayer {
    func setupShadow() {
        shadowColor = UIColor.black.cgColor
        shadowOpacity = 0.5
        shadowRadius = 10
        shadowOffset = CGSize(width: 0, height: 2)
    }
}
