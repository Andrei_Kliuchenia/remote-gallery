//
//  ViewController.swift
//  RemoteGallery
//
//  Created by Andrei Kliuchenia on 20.02.22.
//

import UIKit
import SafariServices


class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var pageControl: UIPageControl!

    var scrollTimer: Timer?

    typealias Dictionary = [String: PhotoItems]
    var photosItems: [PhotoItems] = []
    var keys: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .yellow
        let xib = UINib(nibName: "GalleryCell", bundle: nil)
        collectionView.register(xib, forCellWithReuseIdentifier: "GalleryCell")
        dataLoader()
        startTimer()
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.estimatedItemSize = .zero
    }

    func dataLoader() {
        let urlString = "http://dev.bgsoft.biz/task/"
        guard let url = URL(string: urlString) else { return }

        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else { return }

            do {
                let dictionary = try JSONDecoder().decode(Dictionary.self, from: data)
                for item in dictionary {
                    if let itemDictionary = dictionary[item.key] {
                        self.photosItems.append(itemDictionary)

                        self.keys.append(item.key)

                    }
                }
                self.photosItems.sort { $0.userName < $1.userName }
            } catch let jsonError {
                print("Failed to decode JSON", jsonError)
            }
        }.resume()
    }
}

extension GalleryViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photosItems.count
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4, animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.4, animations: {
            cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        })
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
        cell.delegate = self
        let item = self.photosItems[indexPath.row]
        DispatchQueue.global().async {
            let urlPathForImage =  "http://dev.bgsoft.biz/task/\(self.keys[indexPath.row]).jpg"
            guard let imageUrl = URL(string: urlPathForImage) else { return }
            guard let imageData = try? Data(contentsOf: imageUrl) else { return }
            DispatchQueue.main.async {
                cell.imageView.image = UIImage(data: imageData)
            }
        }
        cell.userName = item.userName
        cell.photoUrlText = item.photoURL
        cell.userUrlText = item.userURL
        cell.imageView.layer.cornerRadius = 15
        cell.contentCellView.layer.setupShadow()
        return cell
    }
}

extension GalleryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = view.bounds.width
        let itemHeight = view.bounds.height
        return CGSize(width: itemWidth, height: itemHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let e = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return e
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        updatePageControl(scrollView: scrollView)
        scrollTimer?.invalidate()
        scrollTimer = nil
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updatePageControl(scrollView: scrollView)
        startTimer()
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        updatePageControl(scrollView: scrollView)
    }

    func updatePageControl(scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.bounds.size.width)
        pageControl.numberOfPages = photosItems.count
        let count = pageControl.numberOfPages
        let currentPageNumber = Int(pageNumber) % count
        pageControl.currentPage = currentPageNumber
    }

    //MARK: - AutoScrolling

    func startTimer() {
         guard scrollTimer == nil else {
             return
         }
        scrollTimer = Timer.scheduledTimer(timeInterval: 5.0,
                                      target: self,
                                      selector: #selector(scrollAutomatically),
                                      userInfo: nil,
                                      repeats: true)
     }

    @objc func scrollAutomatically() {

        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        let contentOffset = collectionView.contentOffset
        UIView.animate(withDuration: 0.4) {
            self.collectionView.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true)
        }
    }
}

//MARK: - CellDelegate

extension GalleryViewController: CellDelegate {

    func openLink(url: String) {
        if let url = URL(string: url) {
            let safariViewController = SFSafariViewController(url: url)
            present(safariViewController, animated: true)
        }
    }
}

protocol CellDelegate {
    func openLink(url: String)
}
