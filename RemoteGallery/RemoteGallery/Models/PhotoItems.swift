//
//  PhotoItems.swift
//  RemoteGallery
//
//  Created by Andrei Kliuchenia on 21.02.22.
//

import Foundation

struct PhotoItems: Codable {
    let photoURL, userURL: String
    let userName: String
    let colors: [String]

    enum CodingKeys: String, CodingKey {
        case photoURL = "photo_url"
        case userURL = "user_url"
        case userName = "user_name"
        case colors
    }
}
